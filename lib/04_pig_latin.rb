def pigify(word)
  vowels = ['a', 'e', 'i', 'o', 'u']
  word.each_char.with_index do |x, index|
    if vowels.include? x.downcase && index == 0
      return "#{word}ay"
    elsif x.casecmp('u') == 0 && word[index - 1].casecmp('q') == 0
      return "#{word[index + 1..-1]}#{word[0..index]}ay"
    elsif vowels.include? x.downcase
      return "#{word[index..-1]}#{word[0...index]}ay"
    end
  end
end

def translate(phrase)
  to_translate = []
  phrase.split(' ').each { |x| to_translate << pigify(x) }
  to_translate.join(' ')
end
