def echo(to_say)
  to_say
end

def shout(to_shout)
  to_shout.upcase
end

def repeat(phrase, repetitions = 2)
  final_phrase = []
  (0...repetitions).each { final_phrase << phrase }
  final_phrase.join(' ')
end

def start_of_word(word, letters = 1)
  word[0...letters]
end

def first_word(phrase)
  phrase.split(' ')[0]
end

def titleize(phrase)
  little_words = ['the', 'over', 'and']
  phrase = phrase.split(' ')
  phrase.each_with_index { |x, index| x.capitalize! unless (little_words.include? x and index != 0) }
  phrase.join(' ')
end
