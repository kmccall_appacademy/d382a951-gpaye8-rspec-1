def add(*numbers)
  numbers.inject(0) { |sum, x| sum + x }
end

def subtract(num1, num2)
  num1 - num2
end

def sum(numbers)
  numbers.inject(0) { |sum, x| sum + x }
end

def multiply(*numbers)
  numbers.inject(1) { |product, x| product * x }
end

def power(base, exponent)
  base**exponent
end

def factorial(number)
  product = 1
  (1..number).each { |x| product = x * product }
  product
end
